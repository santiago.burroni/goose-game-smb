//Write a program that runs through the board spaces and prints the rules of the game according to the board space.
//For all the spaces you must print Stay in space {{space you fell in}},
//but for multiples of six print Move two spaces forward, and for space 6 add the rule The Bridge: Go to space 12.

class Main {
    companion object {
        private const val AMOUNT_OF_TILES = 63
        private const val SIXTH_TILE_LABEL = "The Bridge: Go to space 12"
        private const val MULTIPLE_OF_SIX_LABEL = "Move two spaces forward"
        private const val DEFAULT_LABEL_TEMPLATE = "Stay in space %s"

        @JvmStatic
        fun main(args: Array<String>) {
            runGooseGameExample()
        }

        //TODO: this actually runs through and explains the result of landing on each tile, so maybe change the name. runGooseGameDemoGame?
        private fun runGooseGameExample() {
            printLabelListToConsole(parseAmountOfTilesToLabelList())
        }

        private fun parseAmountOfTilesToLabelList() = (1..AMOUNT_OF_TILES).map { parseLabelFromCurrentNumber(it) }

        //TODO: what's the best syntax to use here? let? run? aaaaaa :thinking:
        private fun parseLabelFromCurrentNumber(number: Int): String = number.run {
            when {
                isSix() -> SIXTH_TILE_LABEL
                isMultipleOfSix() -> MULTIPLE_OF_SIX_LABEL
                else -> String.format(DEFAULT_LABEL_TEMPLATE, number)
            } + "\n"
        }

        //TODO: this should go on the Int class, but because of obvious reasons i cannot. maybe make a wrapper around it?
        private fun Int.isSix() = this == 6

        private fun Int.isMultipleOfSix() = this % 6 == 0

        private fun printLabelListToConsole(list: List<String>) {
            list.forEach { print(it) }
        }
    }
}